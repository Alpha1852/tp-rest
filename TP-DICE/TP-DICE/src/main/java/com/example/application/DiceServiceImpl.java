package com.example.application;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class DiceServiceImpl implements DiceService{
    @Override
    public int launch() {
        Random r=new Random();
        return r.nextInt(6)+1;
    }
}
