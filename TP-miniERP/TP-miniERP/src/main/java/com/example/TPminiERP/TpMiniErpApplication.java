package com.example.TPminiERP;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TpMiniErpApplication {

	public static void main(String[] args) {
		SpringApplication.run(TpMiniErpApplication.class, args);
	}

}
