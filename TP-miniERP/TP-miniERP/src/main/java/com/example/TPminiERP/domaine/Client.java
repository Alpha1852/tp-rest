package com.example.TPminiERP.domaine;

public class Client {

    private Adresse adresse;

    private String nom;

    private String dateNaissance;

    private String prenom;

    private String email;


    public Client(Adresse adresse, String nom, String dateNaissance, String prenom, String email) {
        this.adresse = adresse;
        this.nom = nom;
        this.dateNaissance = dateNaissance;
        this.prenom = prenom;
        this.email = email;
    }

    public Client() {
    }

    public Adresse getAdresse() {
        return adresse;
    }

    public void setAdresse(Adresse adresse) {
        this.adresse = adresse;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(String dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
