package com.example.exposition;

import com.example.application.IMoteurCalcul;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

@RestController
@RequestMapping("/api/v1/calcul")
public class CalculController {

    @Autowired
    IMoteurCalcul moteurCalcul;

    @GetMapping("/sum/{a}/{b}")
    public double sum(@PathVariable("a") double a,@PathVariable("b") double b){
        return moteurCalcul.sum(a,b);
    }

    @GetMapping("/times/{a}/{b}")
    public double times(@PathVariable("a") double a,@PathVariable("b") double b){
        return moteurCalcul.times(a,b);
    }

    @GetMapping("/minus/{a}/{b}")
    public double minus(@PathVariable("a") double a,@PathVariable("b") double b){
        return moteurCalcul.minus(a,b);
    }

    @GetMapping("/square/{a}")
    public double square(@PathVariable("a") double a){
        return moteurCalcul.square(a);
    }

    @GetMapping("/divided/{a}/{b}")
   public double divided(@PathVariable("a") double a,@PathVariable("b") double b){
        if(b!=0){
            return moteurCalcul.divided(a,b);
        }else{
            return 0.0;
        }
   }
}
